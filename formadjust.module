<?php

/**
 * @file
 * Enables to perform a wide variety of modyfications on forms.
 */

/**
 * Implementation of hook_help().
 */
function formadjust_help( $path, $arg ) {
  switch ( $path ) {
    case 'admin/help#formadjust':
      return t("<p>The \"Form adjust\" module allows admin to alter specific form item's attribute value.</p>");
  }
}

/**
 * Implementation of hook_perm().
 */
function formadjust_perm() {
  return array( 'administer formadjust' );
}

/**
 * Implementation of hook_menu().
 */
function formadjust_menu() {
  $items = array();
  $items['admin/settings/formadjust/add/%'] = array(
    'title' => t('Add a new rule'),
    'page callback' => 'formadjust_add_rule',
    'page arguments' => array( 4 ),
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/formadjust/edit/%/%'] = array(
    'title' => t('Edit existing rule'),
    'page callback' => 'formadjust_edit_rule',
    'page arguments' => array( 5, 4 ),
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/formadjust/delete/%/%'] = array(
    'title' => t('Delete existing rule?'),
    'page callback' => 'formadjust_delete_rule',
    'page arguments' => array( 5, 4 ),
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/formadjust'] = array(
    'title' => t('Form adjust'),
    'description' => 'Alter forms.',
    'page callback' => 'formadjust_list_fields',
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/formadjust/list'] = array(
    'title' => t('List fields'),
    'description' => 'List altered fields.',
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/settings/formadjust/settings'] = array(
    'title' => t('Settings'),
    'description' => 'Change Form adjust settings.',
    'page callback' => 'formadjust_settings',
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/settings/formadjust/form/%'] = array(
    'title' => t('Form rules'),
    'page callback' => 'formadjust_list_form_fields',
    'page arguments' => array( 4 ),
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/formadjust/view/%'] = array(
    'title' => t('View rules'),
    'page callback' => 'formadjust_view_rules',
    'page arguments' => array( 4 ),
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/formadjust/auto-property'] = array(
    'title' => t('Form item property name autocomplete/%'),
    'page callback' => 'formadjust_property_autocomplete',
    'page arguments' => array( 4 ),
    'access callback' => 'user_access',
    'access arguments' => array( 'administer formadjust' ),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function formadjust_theme() {
  return array(
    'formadjust_list_fields' => array(
      'arguments' => array('rules' => array()),
    ),
    'formadjust_list_form_fields' => array(
      'arguments' => array('rules' => array()),
    ),
    'formadjust_view_rules' => array(
      'arguments' => array('rules' => array(), 'field_info' => ''),
    ),
    'formadjust_fieldset' => array(
      'arguments' => array('classes' => array(), 'title' => '', 'content' => ''),
    ),
    'formadjust_add_rule' => array(
      'arguments' => array('form_id' => '', 'field_info' => ''),
    ),
    'formadjust_delete_rule' => array(
      'arguments' => array('form_id' => '', 'rlid' => NULL, 'field_info' => ''),
    ),
    'formadjust_settings' => array(
      'arguments' => array('form_id' => ''),
    ),
  );
}

/**
 * Implementation of hook_form_alter().
 */
function formadjust_form_alter( $form, $form_state, $form_id ) {
  // Add link to rule add form in every form field description.
  if ( user_access( 'administer formadjust' ) && variable_get('formadjust_append_links', 1) ) {
    _formadjust_array_walk_recursive( $form, _formadjust_append_link, array($form_id) );
  }
  
  global $user;
  $user_roles = _formadjust_get_user_roles( $user->uid );
  $rules      = formadjust_get_rules();
  foreach ( $rules as $rule ) {
    // If form name matches and a rule applies to curent user (the rule is defined for every user or the role attached to curent user).
    if ( strcmp($rule['form_name'], $form_id) === 0
        && ($rule['rid'] == 0 || in_array($rule['rid'], $user_roles)) ) {
      // Execute the rule.
      _formadjust_form_execute( $form, $rule, _formadjust_value_execute );
    }
  }
}

/**
 * Page callback with "add rule" form.
 *
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return 
 *   Formated page.
 */
function formadjust_add_rule( $field_info = '' ) {
  return theme( 'formadjust_add_rule', 'formadjust_add_rule_form', $field_info );
}

/**
 * Form builder to add rules.
 */
function formadjust_add_rule_form( &$form_state, $field_info ) {
  // Get form name, breadcrumb and field name.
  formadjust_explode_field_info( $field_info, $form_name, $breadcrumb, $field_name );
  $roles = _formadjust_get_roles();
  $roles[0] = t('All roles');
  $form = array();
  $form['breadcrumb'] = array(
    '#type' => 'value',
    '#value' => $breadcrumb,
  );
  $form['form_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Form identificator'),
    '#value' => check_plain( $form_name ),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name'),
    '#value' => check_plain( $field_name ),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['property_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Property name'),
    '#description' => t('Property name you want to change with preceding "#".'),
    '#autocomplete_path' => 'admin/settings/formadjust/auto-property',
    '#required' => TRUE,
  );
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Value'),
    '#description' => t('Property\'s new value as PHP code (omit the "&lt;?php ?&gt;" tags). The property will get the value returned by "return" statement. You can access previous property value with $s.'),
    '#required' => TRUE,
  );
  $form['roles'] = array(
    '#type' => 'select',
    '#title' => t('Roles'),
    '#description' => t('Select roles this rule will affect.'),
    '#default_value' => 'all',
    '#options' => $roles,
    '#multiple' => TRUE,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save rule'),
  );
  return $form;
}

/**
 * Add rule form validatation.
 */
function formadjust_add_rule_form_validate( $form, &$form_state ) {
  $form_values = $form_state['values'];
  // Check PHP syntax.
  if ( !_formadjust_check_php_syntax( $form_values['value'] ) ) {
    form_set_error( 'value', t('Check your PHP syntax.') );
  }
  
  if ( drupal_substr( $form_values['property_name'], 0, 1 ) != '#' ) {
    form_set_error( 'property_name', t('Property name must be preceded with "#".') );
  }
  
  $form_name     = $form_values['form_name'];
  $field_name    = $form_values['field_name'];
  $property_name = $form_values['property_name'];
  $breadcrumb    = $form_values['breadcrumb'];
  // Check if there are existing rules for this property, field and roles.
  foreach ( $form_values['roles'] as $role ) {
    $count = db_fetch_object( db_query( "SELECT count(rlid) as count FROM {formadjust_rules} WHERE form_name='%s' AND field_name='%s' AND property_name='%s' AND breadcrumb='%s' AND rid=%d", $form_name, $field_name, $property_name, $breadcrumb, $role ) )->count;
    if ( $count ) {
      form_set_error( 'roles', t('The rule for such property in this field is already set for one of the roles.') );
      break;
    }        
  }
}

/**
 * Add rule form submission.
 */
function formadjust_add_rule_form_submit( $form, &$form_state ) {
  $form_values = $form_state['values'];

  $form_name     = $form_values['form_name'];
  $field_name    = $form_values['field_name'];
  $property_name = $form_values['property_name'];
  $breadcrumb    = $form_values['breadcrumb'];
  $value         = $form_values['value'];
  $roles         = $form_values['roles'];
  // Add a new rule for every specified role.
  if ( formadjust_insert_rule( $form_name, $field_name, $property_name, $breadcrumb, $value, $roles ) ) {
    drupal_set_message( t('The rule has been added.') );
  }
  else {
    drupal_set_message( t('Failed to add a new rule.'), 'warning' );
  }
}

/**
 * Theme the add rule page.
 * 
 * @param string $form_id
 *   Form ID to build.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return 
 *   Formated page.
 * 
 * @ingroup themeable
 */
function theme_formadjust_add_rule( $form_id, $field_info ) {    
  $output = sprintf( '<p>%s</p>', t( 'Return to the !rule_list.', array( '!rule_list' => l(t('rule list'), 'admin/settings/formadjust/view/'. $field_info) ) ) );
  $output .= drupal_get_form( $form_id, $field_info );
  return $output;
}

/**
 * Page callback with "edit rule" form.
 *
 * @param int $rlid
 *   Rule ID.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return
 *   Formated page.
 */
function formadjust_edit_rule( $rlid, $field_info ) {
  return drupal_get_form( 'formadjust_edit_rule_form', $rlid, $field_info );
}

/**
 * Form builder to edit rules.
 *
 * @param int $rlid
 *   Rule ID.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 */
function formadjust_edit_rule_form( &$form_state, $rlid, $field_info ) {
  $rule = formadjust_load_rule( $rlid );
  $form = array();
  $form['rlid'] = array(
    '#type' => 'value',
    '#value' => $rlid,
  );
  $form['field_info'] = array(
    '#type' => 'value',
    '#value' => $field_info,
  );
  $form['form_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Form identificator'),
    '#value' => check_plain( $rule['form_name'] ),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name'),
    '#value' => check_plain( $rule['field_name'] ),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['property_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Property name'),
    '#value' => check_plain( $rule['property_name'] ),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Value'),
    '#description' => t('Property\'s new value as PHP code (omit the "&lt;?php ?&gt;" tags). The property will get the value returned by "return" statement. You can access previous property value with $s.'),
    '#default_value' => $rule['value'],
    '#required' => TRUE,
  );
  $form['role_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Role name'),
    '#value' => $rule['rid'] ? check_plain($rule['role_name']) : t('All roles'),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );
  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update rule'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  return $form;
}

/**
 * Edit rule form validatation.
 */
function formadjust_edit_rule_form_validate( $form, &$form_state ) {
  $form_values = $form_state['values'];
  
  if ( $form_values['op'] == t('Cancel') ) {
    $path = 'admin/settings/formadjust/view/'. $form_values['field_info'];
    drupal_goto( $path );
    return;
  }
  if ( $form_values['op'] == t('Delete') ) {
    $path = 'admin/settings/formadjust/delete/'. $form_values['field_info'] .'/'. $form_values['rlid'];
    drupal_goto( $path );
    return;
  }
  
  // Check PHP syntax.
  if ( !_formadjust_check_php_syntax( $form_values['value'] ) ) {
    form_set_error( 'value', t('Check your PHP syntax.') );
  }
}

/**
 * Edit rule form submission.
 */
function formadjust_edit_rule_form_submit( $form, &$form_state ) {
  $form_values = $form_state['values'];
  
  if ( $form_values['op'] == t('Update rule') ) {
    $affected = formadjust_update_rule( $form_values['rlid'], $form_values['value'] );
    if ( $affected ) {
      drupal_set_message( t('The rule has been updated.') );
    }
    else {
      drupal_set_message( t('Failed to update the rule.'), 'warning' );
    }
  }
}

/**
 * Page callback with "delete rule" form.
 * 
 * @param int $rlid
 *   Rule ID.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return
 *   Formated page.
 */
function formadjust_delete_rule( $rlid, $field_info ) {
  return theme( 'formadjust_delete_rule', 'formadjust_delete_rule_form', $rlid, $field_info );
}

/**
 * Form builder to delete rules.
 *
 * @param int $rlid
 *   Rule ID.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 */
function formadjust_delete_rule_form( &$form_state, $rlid, $field_info ) {
  $form = array();
  $form['rlid'] = array(
    '#type' => 'value',
    '#value' => $rlid,
  );
  $form['field_info'] = array(
    '#type' => 'value',
    '#value' => $field_info,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  return $form;
}

/**
 * Delete rule form validatation.
 */
function formadjust_delete_rule_form_validate( $form, &$form_state ) {
  $form_values = $form_state['values'];
  
  if ( $form_values['op'] == t('Cancel') ) {
    $path = 'admin/settings/formadjust/edit/'. $form_values['field_info'] .'/'. $form_values['rlid'];
    drupal_goto( $path );
    return;
  }
  if ( $form_values['op'] == t('Delete') ) {
    $rule = formadjust_load_rule( $form_values['rlid'] );
    if ( $rule === FALSE ) {
      drupal_set_message( t('Failed to delete the rule: not found.'), 'warning' );
      $path = 'admin/settings/formadjust/view/'. $form_values['field_info'];
      drupal_goto( $path );
      return;
    }
  }
}

/**
 * Delete rule form submission.
 */
function formadjust_delete_rule_form_submit( $form, &$form_state ) {
  $form_values = $form_state['values'];
  
  if ( $form_values['op'] == t('Delete') ) {
    $count = formadjust_remove_rule( $form_values['rlid'] );
    if ( $count ) {
      drupal_set_message( t('The rule has been deleted.') );
    }
    else {
      drupal_set_message( t('Failed to deleted the rule.'), 'warning' );
    }
    
    // If any rule left for this filed go to the rule's view.
    // If no rules left for this field go back to formadjust administration page.
    formadjust_explode_field_info( $form_values['field_info'], $form_name, $breadcrumb, $field_name );
    $remaining = count( formadjust_find_rules( $form_name, $breadcrumb, $field_name ) );
    if ( $remaining ) {
      $path = 'admin/settings/formadjust/view/'. $form_values['field_info'];
    }
    else {
      $path = 'admin/settings/formadjust';
    }
    drupal_goto( $path );
    return;
  }
}

/**
 * Theme the delete page (question).
 * 
 * @param string $form_id
 *   Form ID to build.
 * @param int $rlid
 *   Rule ID.
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return
 *   Formated page.
 * 
 * @ingroup themeable
 */
function theme_formadjust_delete_rule( $form_id, $rlid, $field_info ) {
  formadjust_explode_field_info( $field_info, $form_name, $breadcrumb, $field_name );
  $str_breadcrumb = $breadcrumb ? ' (breadcrumb: %breadcrumb)' : '';
  $output  = sprintf( '<p>%s</p>', t( "Rule defined for form %form_name, field %field_name$str_breadcrumb.", array('%form_name' => $form_name, '%field_name' => $field_name, '%breadcrumb' => $breadcrumb) ) );
  $output .= sprintf( '<p>%s</p>', t('Are you sure you want to delete rule?') );
  $output .= drupal_get_form( $form_id, $rlid, $field_info );
  return $output;
}

/**
 * Page callback with fields list.
 * 
 * @return Formated page.
 */
function formadjust_list_fields() {
  $fields = formadjust_get_fields();
  return theme( 'formadjust_list_fields', $fields );
}

/**
 * Theme the fields list.
 * 
 * @ingroup themeable
 */
function theme_formadjust_list_fields( $fields ) {
  $rows = array();
  foreach ( $fields as $row ) {
    $link = implode( ',', array( $row['form_name'], $row['breadcrumb'], $row['field_name'] ) );
    $breadcrumb = explode( ',', $row['breadcrumb'] );
    $breadcrumb = implode( ', ', $breadcrumb );
    $rows[] = array(
      l( check_plain( $row['form_name'] ), 'admin/settings/formadjust/form/'. $row['form_name'] ),
      check_plain( $breadcrumb ),
      check_plain( $row['field_name'] ),
      l( t('View'), 'admin/settings/formadjust/view/'. $link ),
    );
  }
  $header = array( t('Form name'), t('Breadcrumb'), t('Field name'), '' );
  
  $output = sprintf( '<p>%s</p>', t('Go to the form containing fields you want to alter if you intend to add a new rule.') );
  $output .= theme( 'table', $header, $rows );
  return $output;
}

/**
 * Page callback with fields list for specific form..
 * 
 * @param string $form_name
 *   Name of form which fields (affected by rules) will be displayed.
 * @return
 *   Formated page.
 */
function formadjust_list_form_fields( $form_name ) {
  $fields = formadjust_get_form_fields( check_plain( $form_name ) );
  return theme( 'formadjust_list_form_fields', $fields );
}

/**
 * Theme the fields list for specific form.
 * 
 * @param array $fields
 *   Array of fields to display.
 * @ingroup themeable
 */
function theme_formadjust_list_form_fields( $fields ) {
  $rows = array();
  foreach ( $fields as $row ) {
    $link = implode( ',', array( $row['form_name'], $row['breadcrumb'], $row['field_name'] ) );
    $breadcrumb = explode( ',', $row['breadcrumb'] );
    $breadcrumb = implode( ', ', $breadcrumb );
    $rows[] = array(
      check_plain( $row['form_name'] ),
      check_plain( $breadcrumb ),
      check_plain( $row['field_name'] ),
      l( t('View'), 'admin/settings/formadjust/view/'. $link ),
    );
  }
  $header = array( t('Form name'), t('Breadcrumb'), t('Field name'), '' );
  
  $output = sprintf( '<p>%s</p>', t('Go to the form containing fields you want to alter if you intend to add a new rule.') );
  $output .= sprintf( '<p>%s</p>', t('List !all_rules.', array('!all_rules' => l(t('all_rules'), 'admin/settings/formadjust'))) );
  $output .= theme( 'table', $header, $rows );
  return $output;
}

/**
 * Page callback with field view.
 * 
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @return
 *   Formated page.
 */
function formadjust_view_rules( $field_info ) {
  formadjust_explode_field_info( $field_info, $form_name, $breadcrumb, $field_name );

  $roles    = _formadjust_get_roles();
  $roles[0] = t('All roles');
  $rules    = array();
  foreach ( $roles as $rid => $role_name ) {
    $role_rules = formadjust_find_rules( $form_name, $breadcrumb, $field_name, $rid );
    
    $rules[] = array(
      'role' => array(
        'rid' => $rid,
        'name' => $role_name,
      ),
      'rules' => $role_rules,
    );
  }
  return theme( 'formadjust_view_rules', $rules, $field_info );
}

/**
 * Theme the field view.
 * 
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @param array $rules
 *   Array of rules.
 * @ingroup themeable
 */
function theme_formadjust_view_rules( $rules, $field_info ) {
  if ( count( $rules ) ) {
    drupal_add_js('misc/collapse.js');
  }
  formadjust_explode_field_info( $field_info, $form_name, $breadcrumb, $field_name );
  $str_breadcrumb = $breadcrumb ? ' (breadcrumb: %breadcrumb)' : '';
  $output = sprintf( '<p>%s</p>', t( "Rules defined for form %form_name, field %field_name$str_breadcrumb.", array('%form_name' => $form_name, '%field_name' => $field_name, '%breadcrumb' => $breadcrumb) ) );
  $output .= sprintf( '<p>%s</p>', t( 'The rules will be applied according to the order set in "Role weight" module.' ) );
  $output .= sprintf( '<p>%s</p>', t( 'Add a !new_rule for this field or return to !field_list.', array( '!new_rule' => l(t('new rule'), 'admin/settings/formadjust/add/'. $field_info), '!field_list' => l(t('field list'), 'admin/settings/formadjust' ) ) ) );
  $header = array( t('Property name'), t('Value'), '' );
  foreach ( $rules as $section ) {
    
    $rows = array();
    // Get rules for role in $section.
    foreach ( $section['rules'] as $rule ) {
      $breadcrumb = implode( ',', array( $rule['form_name'], $rule['breadcrumb'], $rule['field_name'] ) );
      // Get value's excerpt.
      $excerpt = drupal_substr($rule['value'], 0, 20);
      if ( drupal_strlen($rule['value']) > drupal_strlen($excerpt) ) {
        $excerpt .= '...';
      }
      $rows[] = array( 
        check_plain($rule['property_name']),
        check_plain($excerpt),
        l( t('Edit/delete'), 'admin/settings/formadjust/edit/'. $breadcrumb .'/'. $rule['rlid'] ),
      );
    }
    
    // Fill fieldset's content (collapse fieldset if it contains no rules).
    $classes = array( 'collapsible' );
    $title = $section['role']['name'];
    if ( count( $rows ) ) {
      $content = theme( 'table', $header, $rows );
    }
    else {
      $classes[] = 'collapsed';
      $content = sprintf( '<p>%s</p>', t('No rules specified for this role.') );
    }
    
    $output .= theme( 'formadjust_fieldset', $classes, $title, $content );
  }
  return $output;
}

/**
 * Theme the field view.
 * 
 * @param array $classes
 *   Array containing classes' name (they will be attached to the fieldset).
 * @param string $title
 *   Fieldset's title.
 * @param string $content
 *   Fieldset's content
 * @return
 *   Formated fieldset.
 */
function theme_formadjust_fieldset( $classes = array(), $title = '', $content = '' ) {
  $classes = implode( ' ', $classes );
  $title = check_plain( $title );
  return sprintf( '<fieldset class="%s"><legend>%s</legend>%s</fieldset>' , $classes, $title, $content );
}

/**
 * Page callback with module settings.
 */
function formadjust_settings() {
  return theme( 'formadjust_settings', 'formadjust_settings_form' );
}

/**
 * Module settings form builder.
 */
function formadjust_settings_form( &$form_state ) {
  $form = array();
  $form['append_links'] = array(
    '#type' => 'radios',
    '#title' => 'Append links in form fields description',
    '#default_value' => variable_get('formadjust_append_links', 1),
    '#options' => array(
      1 => t('Append links'),
      0 => t('Do not append links'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Module settings form submission handle.
 */
function formadjust_settings_form_submit( $form, &$form_state ) {
  variable_set('formadjust_append_links', $form_state['values']['append_links'] );
}

/**
 * Theme the module settings form.
 *
 * @param string $form_id
 *   The ID of form to build.
 * @return Themed page.
 */
function theme_formadjust_settings( $form_id ) {
  $output = drupal_get_form( $form_id );
  return $output;
}

/**
 * Insert rule to the database.
 *
 * @param string $form_name
 *   Form name.
 * @param string $field_name
 *   Field name.
 * @param string $property_name
 *   Property name.
 * @param string $breadcrumb
 *   Comma-separated list of keys in form array you must to pass to get to the field.
 * @param string $value
 *   PHP code (returned value will be attached to the property).
 * @param array $roles
 *   Array of role IDs.
 * @return
 *   Number of inserted rules.
 */
function formadjust_insert_rule( $form_name, $field_name, $property_name, $breadcrumb, $value, $roles ) {
  $count = 0;
  foreach ( $roles as $role ) {
    $count += db_query( "INSERT INTO {formadjust_rules} (form_name, field_name, property_name, breadcrumb, value, rid) VALUES ( '%s', '%s', '%s', '%s', '%s', %d )", $form_name, $field_name, $property_name, $breadcrumb, $value, $role );
  }
  return $count;
}

/**
 * Update rule's value.
 *
 * @param int $rlid
 *   Rule ID.
 * @param string $value
 *   PHP code (returned value will be attached to the property).
 * @return
 *   Number of rules affected.
 */
function formadjust_update_rule( $rlid, $value ) {
  return db_query( "UPDATE {formadjust_rules} SET value='%s' WHERE rlid=%d", $value, $rlid );
}

/**
 * Delete a rule.
 *
 * @param int $rlid
 *   Rule ID.
 * @return
 *   Number of rules deleted.
 */
function formadjust_remove_rule( $rlid ) {
  return db_query( 'DELETE FROM {formadjust_rules} WHERE rlid=%d', $rlid );
}

/**
 * Get all rules.
 *
 * @return All rules, with order keeped (first 'for all', next according to the role's weight.
 */
function formadjust_get_rules() {
  $rules = array();
  $result = db_query( 'SELECT f.*, r.name AS role_name FROM {formadjust_rules} f LEFT JOIN {role} r USING (rid) WHERE rid = 0 ORDER BY r.weight DESC, f.form_name ASC, f.field_name ASC, f.property_name ASC' );
  while ( $rule = db_fetch_array( $result ) ) {
    $rules[ $rule['rlid'] ] = $rule;
  }
  $result = db_query( 'SELECT f.*, r.name AS role_name FROM {formadjust_rules} f LEFT JOIN {role} r USING (rid) WHERE rid <> 0 ORDER BY r.weight DESC, f.form_name ASC, f.field_name ASC, f.property_name ASC' );
  while ( $rule = db_fetch_array( $result ) ) {
    $rules[ $rule['rlid'] ] = $rule;
  }
  return $rules;
}

/**
 * Load rule.
 *
 * @param int $rlid
 *   Rule ID.
 * @return
 *   Array containing the rules properties.
 */
function formadjust_load_rule( $rlid ) {
  $rule = db_fetch_array( db_query( 'SELECT f.*, r.name AS role_name FROM {formadjust_rules} f LEFT JOIN {role} r USING (rid) WHERE rlid = %d', $rlid ) );
  return $rule;
}

/**
 * Get all fields with at least one rule.
 *
 * @return Array of fields.
 */
function formadjust_get_fields() {
  $fields = array();
  $result = db_query( 'SELECT DISTINCT f.form_name, f.breadcrumb, f.field_name FROM {formadjust_rules} f ORDER BY f.form_name ASC, f.field_name ASC, f.property_name ASC' );
  while ( $field = db_fetch_array( $result ) ) {
    $fields[] = $field;
  }
  return $fields;
}

/**
 * Get all fields with at least one rule.
 *
 * @return Array of fields.
 */
function formadjust_get_form_fields( $form_name ) {
  $fields = array();
  $result = db_query( "SELECT DISTINCT f.form_name, f.breadcrumb, f.field_name FROM {formadjust_rules} f WHERE form_name='%s' ORDER BY f.form_name ASC, f.field_name ASC, f.property_name ASC", $form_name );
  while ( $field = db_fetch_array( $result ) ) {
    $fields[] = $field;
  }
  return $fields;
}

/**
 * Get all rules matching form name, breadcrumb, field name [and role ID].
 *
 * @param string $form_name
 *   Form name.
 * @param string $breadcrumb
 *   Comma-separated list of keys in form array you must to pass to get to the field.
 * @param string $field_name
 *   Field name.
 * @param int $rid
 *   Role ID.
 * @return
 *   Array of rules.
 */
function formadjust_find_rules( $form_name, $breadcrumb, $field_name, $rid = NULL ) {
  
  if ( is_null( $rid ) ) {
    $result = db_query( "SELECT f.* FROM {formadjust_rules} f WHERE form_name='%s' AND breadcrumb='%s' AND field_name='%s' ORDER BY f.form_name ASC, f.field_name ASC, f.property_name ASC", $form_name, $breadcrumb, $field_name );
  }
  else {
    $result = db_query( "SELECT f.* FROM {formadjust_rules} f WHERE rid=%d AND form_name='%s' AND breadcrumb='%s' AND field_name='%s' ORDER BY f.form_name ASC, f.field_name ASC, f.property_name ASC", $rid, $form_name, $breadcrumb, $field_name );
  }
  $rules = array();
  while ( $rule = db_fetch_array( $result ) ) {
    $rules[ $rule['rlid'] ] = $rule;
  }
  return $rules;
}

/**
 * Explode field info.
 *
 * @param string $field_info
 *   Comma-separated path to the field, beggining with form name, through form aray keys, ending with field name.
 * @param string $form_name
 *   Form name will be assined to this variable.
 * @param string $breadcrumb
 *   Breadcrumb will be assined to this variable.
 * @param string $field_name
 *   Field name will be assined to this variable.
 */
function formadjust_explode_field_info( $field_info, &$form_name, &$breadcrumb, &$field_name ) {
  $breadcrumb = explode( ',', $field_info );
  $form_name = array_shift( $breadcrumb );
  $field_name = array_pop( $breadcrumb );
  $breadcrumb = implode( ',', $breadcrumb );
}

/**
 * Autocomplete function for property names.
 *
 * @param string $property_name_fragment
 *   Fragment of property name to match.
 */
function formadjust_property_autocomplete( $property_name_fragment = '' ) {
  $search = $property_name_fragment;
  
  $properties = array( '#access', '#action', '#after_build', '#ahah', '#attributes', '#autocomplete_path', '#button_type', '#collapsed', '#collapsible', '#cols', '#default_value', '#delta', '#description', '#disabled', '#element_validate', '#executes_submit_callback', '#field_prefix', '#field_suffix', '#maxlength', '#method', '#multiple', '#options', '#parents', '#prefix', '#redirect', '#required', '#return_value', '#rows', '#size', '#src', '#submit', '#suffix', '#theme', '#title', '#tree', '#type', '#validate', '#value', '#weight' );
  
  $matches = array();
  foreach ( $properties as $name ) {
    if ( strpos( $name, $search ) !== FALSE ) {
      $matches[ $name ] = $name;
    }
  }
  
  print drupal_to_js($matches);

  exit();
}

/**
 * Walk through an array and call $function for every leaf (array item not being an array).
 *
 * @param array $array
 *   Array to walk through.
 * @param function $function
 *   Function to call for every leaf.
 * @param array $breadcrumb
 *   Array of elements passed from the root.
 */
function _formadjust_array_walk_recursive( &$array, $function, $breadcrumb = array() ) {
  if ( !function_exists( $function ) ) return;
  // If current item is field element (has '#type' set) and there is no '#descripion' in it, define description.
  if ( !isset( $array['#description'] ) && isset( $array['#type'] ) ) { //&& in_array( $array['#type'], $types ) ) {
    $array['#description'] = NULL;
  }
  foreach ( $array as $key => $value ) {
  	$value =& $array[$key];
    // Keep breadcrumb (used to generate "add rule" link).
    $bcrumb = array_merge( $breadcrumb, array($key) );
    if ( is_array( $value ) ) {
      _formadjust_array_walk_recursive( $value, $function, $bcrumb );
    }
    else {
      // Element is a leaf, call function.
      $function( $bcrumb, $value );
    } 
    unset( $value );
  }
}

/**
 * Add text to the item if it's key is '#description'.
 *
 * @param array $breadcrumb
 *   Array of elements passed from the root.
 * @param string $item
 *   Item to operate on (leaf passed from _formadjust_array_walk_recursive()).
 */
function _formadjust_append_link( $field_info, &$item ) {
  $key = array_pop( $field_info );
  $link_add = 'admin/settings/formadjust/add/'. implode( ',', $field_info );
  $link_view = 'admin/settings/formadjust/view/'. implode( ',', $field_info );
  if ( strcmp( $key, '#description' ) === 0 ) {
    $desc = array( $item, t( '!add or !view this field\'s rules.', array( '!add' => l(t('Add'), $link_add), '!view' => l(t('view'), $link_view) ) ) ); 
    $item = implode( ' ', $desc );
  }
}

/**
 * Execute a code passed in $value and assing value it returns to the $item[$property_name].
 *
 * @param array $item
 *   Array which property will get affected.
 * @param string $property_name
 *   Property which value will get affected.
 * @param string $value
 *   PHP code to execute.
 */
function _formadjust_value_execute( &$item, $property_name, $value ) {
  // Make $s accessible.
  $s = serialize( $item[$property_name] );
  $pass = '$s = unserialize(\''. $s .'\');';
  // Execute.
  $item[$property_name] = eval( $pass . $value );
}

/**
 * Find an element in $array with path described in $rule['breadcrumb'] and call $function for it.
 *
 * @param array $array
 *   Usually whole form structure.
 * @param array $rule
 *   The rule array.
 * @param function $function
 *   Function to call on found element.
 */
function _formadjust_form_execute( &$array, $rule, $function ) {
  $breadcrumb = array();
  // Explode breadcrumb if there is any (we don't want to have an array with single, empty value after explode).
  if ( drupal_strlen( $rule['breadcrumb'] ) ) {
    $breadcrumb = explode( ',', $rule['breadcrumb'] );
  }
  $breadcrumb[]  = $rule['field_name'];
  $property_name = $rule['property_name'];
  $value         = $rule['value'];
  // How deep
  $count         = count( $breadcrumb );
  
  // Logic of a "for" loop below:
  // Step 0:                   $item[0] array element becomes an reference to form array
  // Step i (starting form 0): If the path to the desired form element is open (that is: if element $item[$i][$breadcrumb[$i]] exists in form nested arrays), do:
  //                             Keep in $item[$i+1] the reference to the deeper form array element (that means: getting closer to the element we have action to perform on)
  // At the end:               We have a reference to the desired element in $item[$count] (if the path in $breadcrumb was correct and the element really exists).
  //
  // Explanation:
  // This level of complication was necessary, because of wrong reference implementation in PHP 5.2.0.
  // Simply: $item = &$item[$breadcrumb[$i] does not work. 
  // Multiple references in array for every loop step solve this problem.     
  
  // Current element.
  $item = array();
  $item[0] = &$array;
  for ( $i = 0; $i < $count; $i++ ) {
    if ( isset( $item[$i][ $breadcrumb[$i] ] ) ) {
      // Go to branch if it is present.
      $item[$i+1] = &$item[$i][ $breadcrumb[$i] ];
    }
    else {
      // End function if you reached end of path or dead end.
      return;
    }
  }
  // If element reached
  // Define property, if it is not set.
  if ( !isset( $item[$count][$property_name] ) ) {
    $item[$count][ $property_name ] = FALSE;
  }
  // Execute.
  $function( $item[$count], $property_name, $value );
}

/**
 * Check PHP syntax.
 *
 * @param string $code
 *   PHP code to check.
 * @return
 *   False, if there is an error in PHP code.
 */
function _formadjust_check_php_syntax( $code ) {
  $execution_sentry = 'return TRUE;';
  return @eval( $execution_sentry . $code );
}

/**
 * Get roles.
 *
 * @return Roles array indexed with role ID.
 */
function _formadjust_get_roles() {
  $roles = roleweight_get_roles();
  $result = array();
  foreach ( $roles as $role ) {
    $result[ $role['rid'] ] = $role['name'];
  }
  return $result;
}

/**
 * Get roles attached to specific user.
 *
 * @param int $uid
 *   User ID.
 * @return
 *   An array of role names attached to specific user.
 */
function _formadjust_get_user_roles( $uid ) {
  $roles = array();
  global $user;
  $roles = array_keys( $user->roles );
  return $roles;
}

function drupal_print_r( $var, $type = 'status' ) {
  drupal_set_message( '<pre>'. htmlentities( print_r( $var, TRUE ) ) .'</pre>', $type );
}