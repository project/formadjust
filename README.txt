
Form Adjust
-----------

To install, place the entire formadjust forlder into you modules directory.
Go to Administer -> Site building -> Modules and enable Form Adjust module.

Usage
-----

The Form Adjust module allows you to make a variety of modyfications on forms available in your Drupal installation. You can change any field's property value, like #title, #description or #value. Every single change is done by adding a new rule.

To add a new rule make sure, that you have "Append links" option enabled in Administer -> Site configuration -> Form adjust -> Settings.

With "Append links" option enabled Form Adjust will append two links to every form field's descriptions: add and view.

Clicking "add" allows you to add a new rule for this particular field (while "view" give you access to all rules applied for this field). Enter the property name (note that displayed suggestions are not limited to the field type, make sure the field supports property you have chosen). Next you will have to define field's new value. The field will get value returned by PHP code you enter into the "Value" text area (omit the <?php ?> tags). You can acces previous property's value with $s variable. The last thing you must do is to pick out user roles the rule have will affect. You can add only one rule for specific property and role.

However, it is possible that specific user will be assigned to more than one role, assume two. Then, if each of this two roles have a rule defined for specific field's property, a conflict will occur. That is why it is so important to set roles ordering using Role Weight module. In such situations a rule defined for more important role will be executed (in fact all rules will get executed, but the last value will remain).

Maintainers
-----------
Adrian Pitwor
